cmake_minimum_required(VERSION 3.3)
project(suffixarray)

find_package(CUDA REQUIRED)
find_package(OpenMP REQUIRED)

set(CUDA_NVCC_FLAGS "${CUDA_NVCC_FLAGS} ")

CUDA_COMPILE(CUDA_OBJECT src/sa_kernel.cu)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp -L /opt/cuda/lib64/ -lcudart -lstdc++ ")

set(SOURCE_FILES
    src/bigram_bucket.c
    src/bucket.h
    src/growing_array.c
    src/growing_array.h
    src/main.c
    src/sort.c
    src/sort.h
    src/suffixarray.c
    src/suffixarray.h
    src/utility.h src/utility.c src/sa_kernel.h src/sa_kernel.cu src/repetition.h src/repetition.c)

add_executable(suffixarray ${SOURCE_FILES} ${CUDA_OBJECT})