# README #

This README will guide you to necessary steps in executing/running the application.

### What is this repository for? ###
* This is a repository for bigram-suffix-sorting, a parallel suffix array construction algorithm, implemented on OpenMP and CUDA
* Current version 1.0
* Reference paper https://drive.google.com/open?id=0B6RaWodjHdjMTWNkMFVoQTFhZFk


### Execution ###
+ Make sure file sa.config is in current working directory
    * This configuration file stores application configuration (inc. parallel options)
* To execute: `$./main /path/to/file`


### Contact ###
* boestari.riko@gmail.com