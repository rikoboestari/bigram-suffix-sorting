/*
 * growing_array.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdlib.h>
#include <stdio.h>

#include "growing_array.h"

#define ARRAY_GROW 1000

void initArray(GrowingArray *growingArray){
	growingArray->array = (int*)malloc(ARRAY_GROW * sizeof(growingArray->array));
	if(growingArray->array==NULL){
		printf("Memory allocation error\n");
		exit(1);
	}
	growingArray->used = 0;
	growingArray->_size = ARRAY_GROW;
}

void insertToArray(GrowingArray *growingArray, int element){
	if (growingArray->used == growingArray->_size) {
		growingArray->_size += ARRAY_GROW;
		int *_tmp = (int *)realloc(growingArray->array, growingArray->_size * sizeof(*_tmp));
		if(_tmp==NULL){
			printf("Memory reallocation error\n");
			exit(EXIT_FAILURE);
		}
		growingArray->array = _tmp;
	}
	growingArray->array[growingArray->used] = element;
	growingArray->used += 1;
}

void resetArray(GrowingArray *growingArray){
	if(growingArray->_size==0){
		initArray(growingArray);
	}else{
		growingArray->used = 0;
	}
}

void destroyArray(GrowingArray *growingArray){
	free(growingArray->array);
	growingArray->used = 0;
	growingArray->_size = 0;
}
