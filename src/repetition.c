/*
 * repetition.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "repetition.h"
#include "utility.h"
#include "sort.h"
#include "growing_array.h"

static void calculateGroupRepetitionOrder(Repetition *repetition, int *textInt, int n, int *sa, int saSize,
                                          int *groupKeys, int *groupValues, int groupSize, int h, int depth);

static void printRepetition(Repetition *repetition);

void detectRepetition(int *textInt, int n, int *sa, int saSize, int h, int depth, Repetition *repetition){
    int lastDistance = 0;
    int i;
    int foundRepeat = 1;
    int nbRepeatGroup = 1;

    int dist = sa[1] - sa[0];
    if(dist > h) {
        foundRepeat = 0;
    }else {
        for (i = 0; i < saSize - 1; i++) {
            int distance = 0;
            if (sa[i + 1] - sa[i] == dist)
                distance = 1;
            if (distance == 0 && lastDistance == 0) {
                foundRepeat = 0;
                break;
            }
            if (distance == 0)
                nbRepeatGroup++;
            lastDistance = distance;
        }
        if(lastDistance == 0)
            foundRepeat = 0;
    }

    if(foundRepeat){
        repetition->size = nbRepeatGroup;
        repetition->patternLength = dist;
        int *idxArray = (int*) malloc (nbRepeatGroup * sizeof(int));
        int *idxN = (int*) malloc (nbRepeatGroup * sizeof(int));
        char *idxOrder = (char*) malloc (nbRepeatGroup * sizeof(char));

        int idx = 0;
        idxArray[idx] = 0;
        for(i=0; i<saSize-1; i++){
            if(sa[i+1] - sa[i] != dist){
                idxN[idx] = i - idxArray[idx] + 1;
                idx++;
                idxArray[idx] = i + 1;
            }
        }
        idxN[idx] = saSize - idxArray[idx];

        for(i=0; i<repetition->size; i++){
            int idx1 = sa[idxArray[i] + idxN[i] - 2];
            int idx2 = sa[idxArray[i] + idxN[i] - 1];
            int offset = h;

            while(idx2+offset<n && textInt[idx2+offset]==textInt[idx1+offset]){
               offset += depth;
            }

            int order;
            if(idx2+offset >= n){
                order = -1;
            }else if(textInt[idx2+offset] < textInt[idx1+offset]){
                order = -1;
            }else{
                order = 1;
            }
            idxOrder[i] = order;
        }

        repetition->idxArray = idxArray;
        repetition->idxN = idxN;
        repetition->idxOrder = idxOrder;
    }
}

void sortRepetition(Repetition *repetition, int *textInt, int textSize, int *sa, int saSize, int h, int depth){
    if(repetition->size == 1){
        //only one group of repetition
        if(repetition->idxOrder[0] == -1)
            reverseArray(sa, saSize);
    }else{
        //more than one group of repetition
        int *copySA = (int*) malloc (saSize * sizeof(int));
        int i;
        for(i=0; i<saSize; i++)
            copySA[i] = sa[i];

        int nbReverse=0, nbNoReverse=0;
        for(i=0; i<repetition->size; i++){
            if(repetition->idxOrder[i] == -1)
                nbReverse++;
            if(repetition->idxOrder[i] == 1)
                nbNoReverse++;
        }
        assert(nbReverse+nbNoReverse == repetition->size);

        int reverse[nbReverse], noReverse[nbNoReverse];
        int countReverse=0, countNoReverse=0;
        for(i=0; i<repetition->size; i++){
            if(repetition->idxOrder[i] == -1)
                reverse[countReverse++] = i;
            else
                noReverse[countNoReverse++] = i;
        }

        //get the order of reverse groups
        int idx = 0;
        if(nbReverse > 0) {
            if (nbReverse == 1) {
                //if reverse group only one
                int tempIdx = repetition->idxArray[reverse[0]];
                int tempSize = repetition->idxN[reverse[0]];
                reverseArray(copySA + tempIdx, tempSize);
                for(i=0; i<tempSize; i++)
                    sa[idx+i] = copySA[tempIdx+i];
                idx += tempSize;
            } else {
                //if reverse group more than one
                int groupKeys[nbReverse];
                calculateGroupRepetitionOrder(repetition, textInt, textSize, copySA, saSize, groupKeys, reverse, nbReverse, h,
                                              depth);
                int nbGroup[nbReverse];
                for (i = 0; i < nbReverse; i++) {
                    nbGroup[i] = repetition->idxN[reverse[i]];
                }

                int flag = 1;
                while (flag) {
                    int cont = 0;
                    for (i = 0; i < nbReverse; i++) {
                        if (nbGroup[i] > 0) {
                            sa[idx] = copySA[repetition->idxArray[reverse[i]] + nbGroup[i] - 1];
                            idx++;
                            nbGroup[i]--;
                            cont++;
                        }
                    }
                    if (cont == 0)
                        flag = 0;
                }
            }
        }

        //get the order of non-reverse groups
        if(nbNoReverse > 0) {
            if (nbNoReverse == 1) {
                //if no reverse group only one
                int tempIdx = repetition->idxArray[noReverse[0]];
                int tempSize = repetition->idxN[noReverse[0]];
                for(i=0; i<tempSize; i++)
                    sa[idx+i] = copySA[tempIdx+i];
            } else {
                //if no reverse group more than one
                idx = saSize - 1;
                int groupKeys[nbNoReverse];
                calculateGroupRepetitionOrder(repetition, textInt, textSize, copySA, saSize, groupKeys, noReverse, nbNoReverse,
                                              h, depth);
                int nbGroup[nbNoReverse];
                for (i = 0; i < nbNoReverse; i++) {
                    nbGroup[i] = repetition->idxN[noReverse[i]];
                }

                int flag = 1;
                while (flag) {
                    int cont = 0;
                    for (i = nbNoReverse-1; i>=0; i--) {
                        if (nbGroup[i] > 0) {
                            sa[idx] = copySA[repetition->idxArray[noReverse[i]] + nbGroup[i] - 1];
                            idx--;
                            nbGroup[i]--;
                            cont++;
                        }
                    }
                    if (cont == 0)
                        flag = 0;
                }
            }
        }
        free(copySA);
    }
}

static void calculateGroupRepetitionOrder(Repetition *repetition, int *textInt, int n, int *sa, int saSize,
                                          int *groupKeys, int *groupValues, int groupSize, int h, int depth){
    GrowingArray partitionIndex;
    GrowingArray partitionSize;
    initArray(&partitionIndex);
    initArray(&partitionSize);
    insertToArray(&partitionIndex, 0);
    insertToArray(&partitionSize, groupSize);

    while (partitionIndex.used>0) {
        int i;
        for(i=0; i<partitionIndex.used; i++){
            int idx = partitionIndex.array[i];
            int size = partitionSize.array[i];

            //update keys
            int j;
            for(j=0; j<size; j++){
                int lastGroupIdx = repetition->idxArray[groupValues[idx + j]] + repetition->idxN[groupValues[idx + j]] - 1;
                if(sa[lastGroupIdx] + h < n)
                    groupKeys[idx + j] = textInt[sa[lastGroupIdx] + h];
                else
                    groupKeys[idx + j] = 0;
            }

            //sort
            quickSortKeysValuesPairs(groupKeys + idx, groupValues + idx, size);
        }

        //update partitionIndex & partitionSize
        int *copyIndex, *copySize, nbCopy;
        nbCopy = partitionIndex.used;
        copyIndex = (int*) malloc (nbCopy * sizeof(copyIndex));
        copySize = (int*) malloc (nbCopy * sizeof(copySize));

        for(i=0; i<partitionIndex.used; i++){
            copyIndex[i] = partitionIndex.array[i];
            copySize[i] = partitionSize.array[i];
        }
        resetArray(&partitionIndex);
        resetArray(&partitionSize);

        int newOffset = -1, newN = 0;
        int previousKey=0;

        for(i=0; i<nbCopy; i++){
            int index = copyIndex[i];
            int nb = copySize[i];
            int j;
            for(j=index; j<index+nb; j++){
                if(j==index){
                    previousKey = groupKeys[j];
                    newOffset = j;
                    newN = 1;
                }else{
                    if(groupKeys[j]==previousKey){
                        newN++;
                    }else{
                        if(newN>1){
                            insertToArray(&partitionIndex, newOffset);
                            insertToArray(&partitionSize, newN);
                            previousKey = groupKeys[j];
                            newOffset = j;
                            newN = 1;
                        }else{
                            previousKey = groupKeys[j];
                            newOffset = j;
                            newN = 1;
                        }
                    }
                }
            }
            if(newN>1){
                insertToArray(&partitionIndex, newOffset);
                insertToArray(&partitionSize, newN);
            }
        }
        free(copyIndex);
        free(copySize);

        h += depth;
    }

    destroyArray(&partitionIndex);
    destroyArray(&partitionSize);
}


void freeRepetition(Repetition *repetition){
    free(repetition->idxArray);
    free(repetition->idxN);
    free(repetition->idxOrder);
}

static void printRepetition(Repetition *repetition){
    printf("Repetition info:\n");
    printf("size=%d pattern length=%d\n", repetition->size, repetition->patternLength);
    printf("element:\n");
    int i;
    for(i=0; i<repetition->size; i++){
        printf("idxArray[%d] idxN[%d] idxOrder[%d]\n", repetition->idxArray[i], repetition->idxN[i], repetition->idxOrder[i]);
    }
}
