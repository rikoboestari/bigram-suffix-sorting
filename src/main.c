/*
 * main.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

#include "suffixarray.h"
#include "utility.h"


int main(int argc, char **argv){
	printf("Usage: main [path-to-file]\n");
	char *filepath = argv[1];
	if(filepath == NULL) {
		printf("No [path-to-file] argument found!\n");
		exit(1);
	}

	struct stat fileStatus;
	stat(filepath, &fileStatus);
	int n = (int)fileStatus.st_size;
	printf("File: %s, size: %d bytes\n", filepath, n);

	int *SA = (int *) malloc (n * sizeof(int));
	buildSuffixArray(filepath, SA);

	char *text = (char*) malloc (n * sizeof(char));
	loadFile(filepath, text, n);
	sufcheck(text, SA, n);

	free(SA);
	free(text);
}



