/*
 * repetition.h for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef SUFFIXARRAY_REPETITION_H
#define SUFFIXARRAY_REPETITION_H

typedef struct {
    int patternLength;
    int *idxArray;
    int *idxN;
    char *idxOrder; //1 for nonreverse order, -1 for reverse order
    int size;
} Repetition;

void detectRepetition(int *textInt, int n, int *sa, int saSize, int h, int depth, Repetition *repetition);

void sortRepetition(Repetition *repetition, int *textInt, int textSize, int *sa, int saSize, int h, int depth);

void freeRepetition(Repetition *repetition);

#endif //SUFFIXARRAY_REPETITION_H
