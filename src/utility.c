/*
 * utility.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "utility.h"


void loadFile(char *filename, char *buffer, long size){
    FILE *pFile;
    long result;

    pFile = fopen(filename, "r");
    if(pFile==NULL){
        printf("File read error: %s\n", filename);
        exit(1);
    }

    //copy the file into buffer
    result = fread(buffer, 1, size, pFile);
    if(result!=size){
        printf("Reading error\n");
        exit(1);
    }

    //close
    fclose(pFile);
}

/**
 * Count the occurrence of each alphabet in the text.
 * It assumes that text using ASCII code where max value of the alphabet is 255
 */
void characterCount(char *text, int textSize, int charCount[], int charCountSize){
    //init charCount
    int i;
    for(i=0; i<charCountSize; i++){
        charCount[i] = 0;
    }
    for(i=0; i<textSize; i++){
        charCount[text[i]+128]++;
    }
}

void characterCountOpenmp(char *text, int textSize, int charCount[], int charCountSize){
    //init charCount
    int i;

    for(i=0; i<charCountSize; i++){
        charCount[i] = 0;
    }

    #pragma omp parallel shared(text, charCount)
    {
        int tempCharCount[charCountSize];
        int j;
        for(j=0; j<charCountSize; j++)
            tempCharCount[j] = 0;

        #pragma omp for
        for (i = 0; i < textSize; i++) {
            tempCharCount[text[i]+128]++;
        }

        for(j=0; j<charCountSize; j++) {
            #pragma omp critical
            charCount[j] += tempCharCount[j];
        }
    }
}

int countNonzeroArray(int array[], int size){
    int count = 0;
    int i;
    for(i=0; i<size; i++){
        if(array[i]>0){
            count++;
        }
    }
    return count;
}

void filterNonzeroArrayIndex(int input[], int size, char *output){
    int i, j=0;
    for(i=0; i<size; i++){
        if(input[i]>0){
            output[j] = (char)i-128;
            j++;
        }
    }
}

int getExponent(int alphabetSize){
    int exponent = 0;
    int temp = 1;
    while(temp<=(alphabetSize+1)){
        exponent++;
        temp *= 2;
    }
    return exponent;
}

int maxDepth(int exponent){
    int x = 1;
    int i;
    for(i=0; i<exponent; i++)
        x *= 2;
    int max = 0;
    int limit = INT_MAX;
    while(limit > x){
        max++;
        limit /= x;
    }
    return max;
}

static int getAlphabetPosition(char *alphabets, int alphabetSize, char search);
/**
 * Replace all characters in text to ease and fasten the computation of bucket number and
 * suffix integer representation.
 *
 * If alphabets only contains four characters (A,C,G,T), where A=65, C=67, G=71, T=84 in ASCII code,
 * this function replaces A value as 0, C value as 1, G value as 2, T value as 3.
 */
void replaceText(char *text, int textSize, char *alphabets, int alphabetSize, int verbose){
    int i, pos;
    for(i=0; i<textSize; i++){
        pos = getAlphabetPosition(alphabets, alphabetSize, text[i]);
        text[i] = (char)pos-128;
    }
    if(verbose==2)
        for(i=0; i<alphabetSize; i++){
            printf("alphabet-%c(%d) is replaced by %c(%d)\n", alphabets[i], alphabets[i], i-128, i-128);
        }
}

void replaceTextOpenmp(char *text, int textSize, char *alphabets, int alphabetSize, int verbose){
    int i;

    #pragma omp parallel for shared(text, alphabets)
    for(i=0; i<textSize; i++){
        int pos = getAlphabetPosition(alphabets, alphabetSize, text[i]);
        text[i] = (char)pos-128;
    }

    if(verbose==2)
        for(i=0; i<alphabetSize; i++){
            printf("alphabet-%c(%d) is replaced by %c(%d)\n", alphabets[i], alphabets[i], i-128, i-128);
        }
}

/**
 * Returns the search alphabet position in array alphabets
 */
static int getAlphabetPosition(char *alphabets, int alphabetSize, char search){
    int pos = -1;
    int i;

    for(i=0; i<alphabetSize; i++)
        if(alphabets[i]==search){
            pos = i;
            break;
        }
    return pos;
}

/**
 * Returns integer value representing suffix at certain index in the text.
 * The function involves the first k character of the suffix, where k-value is depth
 *
 * If k=3, and the number of alphabets is 4, then we find the least factor of two greater
 * than number of alphabets, in this case it's 8.
 * The function goes like this:
 * 		text[i] * 8^2 + text[i+1] * 8^1 + text[i+2] * 8^0
 *
 * The factor of two is used as the multiplication factor since we can use bit shifting
 * operator to increase performance.
 */
static int intRep(const char *text, int textSize, int index, int exponent, const int depth){
    int sum = 0;
    int i, pos, xpValue;

    for(i=0; i<depth; i++){
        if(index+i < textSize){
            pos = text[index+i]+128 + 1;
            xpValue = 1;
            xpValue = xpValue << (exponent * (depth-1-i));
            sum += pos * xpValue;
        }else{
            break;
        }
    }

    return sum;
}

void calculateIntegerRepresentation(const char *text, int textSize, int exponent, const int depth,
                                    int *textInt, int enableParallellism){
    int i;
    #pragma omp parallel for if(enableParallellism)
    for(i=0; i<textSize; i++){
        int rep = intRep(text, textSize, i, exponent, depth);
        textInt[i] = rep;
    }
}

void reverseArray(int *array, int n){
    int i;
    int half = n/2;
    for(i=0; i<half; i++){
        int temp = array[i];
        array[i] = array[n-1-i];
        array[n-1-i] = temp;
    }
}

