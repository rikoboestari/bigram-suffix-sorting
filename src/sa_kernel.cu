/*
 * sa_kernel.cu for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


extern "C" {
    #include "sa_kernel.h"
}

#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>


__global__ void replaceDepthLengthTextToInt(char *text, int n, int exponent, int depth, int *textInt){
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while(tid < n){
		int i, pos, xpValue, sum=0;
		for(i=0; i<depth; i++){
			if(tid+i < n){
				pos = text[tid+i]+128 + 1;
				xpValue = 1;
				xpValue = xpValue << (exponent * (depth-1-i));
				sum += pos * xpValue;
			}else{
				break;
			}
		}
		textInt[tid] = sum;
		tid += blockDim.x * gridDim.x;
	}
}

__global__ void replaceTextKernel(char *text, int n, char *alphabets, const int alphabetSize){
	__shared__ char alp[128];
	if(threadIdx.x < alphabetSize)
		alp[threadIdx.x] = alphabets[threadIdx.x];
	__syncthreads();

	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	while(tid < n){
		int i;
		for(i=0; i<alphabetSize; i++){
			if(text[tid]==alp[i]){
				break;
			}
		}
		text[tid] = (char)i-128;
		tid += blockDim.x * gridDim.x;
	}
}

extern "C"
void calculateTextInt(char *text, int n, int exponent, int depth, int *textInt, int cudaBlock, int cudaThreadPerBlock){
	char *devText;
	int *devTextInt;
	cudaMalloc((void**)&devText, n * sizeof(char));
	cudaMalloc((void**)&devTextInt, n * sizeof(int));
	cudaMemcpy(devText, text, n * sizeof(char), cudaMemcpyHostToDevice);

	replaceDepthLengthTextToInt<<<cudaBlock, cudaThreadPerBlock>>>(devText, n, exponent, depth, devTextInt);

	cudaMemcpy(textInt, devTextInt, n * sizeof(int), cudaMemcpyDeviceToHost);
	cudaFree(devText);
	cudaFree(devTextInt);
}

extern "C"
void replaceTextCuda(char *text, int n, char *alphabets, int alphabetSize, int cudaBlock, int cudaThreadPerBlock){
	char *devText, *devAlphabets;
	cudaMalloc((void**)&devText, n * sizeof(char));
	cudaMalloc((void**)&devAlphabets, alphabetSize * sizeof(char));
	cudaMemcpy(devText, text, n * sizeof(char), cudaMemcpyHostToDevice);
	cudaMemcpy(devAlphabets, alphabets, alphabetSize * sizeof(char), cudaMemcpyHostToDevice);

	replaceTextKernel<<<cudaBlock, cudaThreadPerBlock>>>(devText, n, devAlphabets, alphabetSize);

	cudaMemcpy(text, devText, n * sizeof(char), cudaMemcpyDeviceToHost);
	cudaFree(devText);
	cudaFree(devAlphabets);
}

extern "C"
void sortKVCuda(int *keys, int *values, int N) {
	thrust::device_vector<int> d_keys(keys, keys + N);
	thrust::device_vector<int> d_values(values, values + N);
	thrust::stable_sort_by_key(d_keys.begin(), d_keys.end(), d_values.begin());

	thrust::copy(d_keys.begin(), d_keys.end(), keys);
	thrust::copy(d_values.begin(), d_values.end(), values);
	d_keys.clear();
	d_keys.shrink_to_fit();
	d_values.clear();
	d_values.shrink_to_fit();
}
