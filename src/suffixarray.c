/*
 * suffixarray.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <omp.h>

#include "bucket.h"
#include "suffixarray.h"
#include "utility.h"
#include "sa_kernel.h"


#define MAX_ALPHABET 256
#define BILLION 1000000000L


static char filepath[255];
static int 	verbose=0,
		verify=0,
		profiler=0,
		lcpThreshold=100,
		enableParallelism=0,
		openmpMaxThread=0,
		radixSortingMinElement=0,
		enableCuda=0,
		cudaMinElement=0,
		cudaBlock=0,
		cudaThreadPerBlock=0;
static int depth;
static int exponent;


static void initConfig(char *configFile);
static void fillISA(int *ISA, int *SA, int saSize, int offset);
static int textPreprocessing(char *text, int size);
static void bucketIndexing(char *text, int size, Bucket *buckets, int bucketSize, int alphabetSize);


void buildSuffixArray(char *filepath, int *SA){
	//timer
	uint64_t runtime, r1;
	struct timespec start, end, c1, c2;

	//init configuration file
	char *configFile = "sa.config";
	initConfig(configFile);
	printf("Configuration:\n");
	printf("\tverbose= %d\n", verbose);
	printf("\tprofiler= %d\n", profiler);
	printf("\tlcpThreshold= %d\n", lcpThreshold);
	printf("\tenableParallelism= %d\n", enableParallelism);
	if(!enableParallelism)
		enableCuda = 0;
	printf("\topenmpMaxThread= %d\n", openmpMaxThread);
	printf("\tradixSortingMinElement= %d\n", radixSortingMinElement);
	printf("\tenableCuda= %d\n", enableCuda);
	printf("\tcudaMinElement= %d\n", cudaMinElement);
	printf("\tcudaBlock= %d\n", cudaBlock);
	printf("\tcudaThreadPerBlock= %d\n", cudaThreadPerBlock);

	//load input file to memory
	struct stat fileStatus;
	stat(filepath, &fileStatus);
	int size = (int)fileStatus.st_size;

	char *text = (char*) malloc (size * sizeof(char));
	if(text==NULL){
		printf("Memory allocation error\n");
		exit(1);
	}
	loadFile(filepath, text, size);

	clock_gettime(CLOCK_MONOTONIC, &start);

	//preprocessing: replace text based on |alphabet| used
	if(profiler==2)
		clock_gettime(CLOCK_MONOTONIC, &c1);
	int alphabetSize = textPreprocessing(text, size);
	if(profiler==2) {
		clock_gettime(CLOCK_MONOTONIC, &c2);
		r1 = BILLION * (c2.tv_sec - c1.tv_sec) + c2.tv_nsec - c1.tv_nsec;
		printf("[RT]Text preprocessing= %llu nanoseconds\n", (long long unsigned int) r1);
	}

	//build bucket index
	if(profiler==2)
		clock_gettime(CLOCK_MONOTONIC, &c1);
	int bucketSize = alphabetSize*alphabetSize + 1;
	Bucket *buckets = (Bucket*) malloc(bucketSize * sizeof(*buckets));
	bucketIndexing(text, size, buckets, bucketSize, alphabetSize);
	if(profiler==2){
		clock_gettime(CLOCK_MONOTONIC, &c2);
		r1 = BILLION * (c2.tv_sec - c1.tv_sec) + c2.tv_nsec - c1.tv_nsec;
		printf("[RT]Bucket indexing= %llu nanoseconds\n", (long long unsigned int) r1);
	}

	//compute every suffix text integer representation
	if(profiler==2)
		clock_gettime(CLOCK_MONOTONIC, &c1);
	int *textInt = (int *) malloc(size * sizeof(*textInt));
	if(textInt==NULL){
		printf("Memory allocation error\n");
		exit(1);
	}
	exponent = getExponent(alphabetSize);
	depth = maxDepth(exponent);
	if(enableParallelism && enableCuda)
		calculateTextInt(text, size, exponent, depth, textInt, cudaBlock, cudaThreadPerBlock);
	else
		calculateIntegerRepresentation(text, size, exponent, depth, textInt, enableParallelism);

	free(text);

	if(profiler==2) {
		clock_gettime(CLOCK_MONOTONIC, &c2);
		r1 = BILLION * (c2.tv_sec - c1.tv_sec) + c2.tv_nsec - c1.tv_nsec;
		printf("[RT]Compute integer representation of every char on string= %llu nanoseconds\n", (long long unsigned int) r1);
	}

	int i;
	int nbBucketA=0, nbBucketB;
	for(i=0; i<alphabetSize; i++)
		nbBucketA += i+1;
	nbBucketB = alphabetSize*alphabetSize - nbBucketA;
	int *bucketsA = (int*) malloc (nbBucketA * sizeof(int));
	int *bucketsB = (int*) malloc (nbBucketB * sizeof(int));

	//get indices of buckets A and buckets B
	int k = 0;
	for(i=0; i<alphabetSize; i++){
		int j;
		for(j=0; j<alphabetSize-i; j++) {
			bucketsA[k] = i * alphabetSize + i + j;
			k++;
		}
	}
	k = 0;
	for(i=0; i<alphabetSize-1; i++){
		int j;
		for(j=0; j<alphabetSize-1-i; j++){
			bucketsB[k] = alphabetSize + i + i*alphabetSize + j*alphabetSize;
			k++;
		}
	}

	//process buckets A
	if(verbose>=1)
		printf("Processing buckets A\n");
	if(profiler>=1)
		clock_gettime(CLOCK_MONOTONIC, &c1);

	#pragma omp parallel for if(enableParallelism)
	for(i=0; i<nbBucketA; i++){
		int idx = bucketsA[i];
		if(verbose==2)
			printf("processing bucket-%d\n", idx);
		if(buckets[idx].used>1){
			sortBucketTypeA(size, textInt, buckets[idx].array, buckets[idx].used, 2, depth, radixSortingMinElement,
							enableParallelism, enableCuda, cudaMinElement);
		}
	}

	if(profiler>=1){
		clock_gettime(CLOCK_MONOTONIC, &c2);
		r1 = BILLION * (c2.tv_sec - c1.tv_sec) + c2.tv_nsec - c1.tv_nsec;
		printf("[RT]Processing buckets A= %llu nanoseconds\n", (long long unsigned int) r1);
	}

	//fill ISA array from buckets A
	//reuse textInt array
	int *ISA = textInt;
	#pragma omp parallel for if(enableParallelism)
	for(i=0; i<size; i++)
		ISA[i] = -1;
	#pragma omp parallel for if(enableParallelism)
	for(i=0; i<nbBucketA; i++){
		int idx = bucketsA[i];
		if(buckets[idx].used>0) {
			fillISA(ISA, buckets[idx].array, buckets[idx].used, buckets[idx].startPosition);
		}
		free(buckets[idx].array);
	}
	free(bucketsA);
	//	fill ISA from bucket containing single last character
	ISA[size-1] = buckets[bucketSize-1].startPosition;
	free(buckets[bucketSize-1].array);

	//process buckets B
	if(verbose>=1)
		printf("Processing buckets B\n");
	if(profiler>=1)
		clock_gettime(CLOCK_MONOTONIC, &c1);

	for(i=0; i<alphabetSize-1; i++){
		int j;
		#pragma omp parallel for private(k) if(enableParallelism)
		for(j=0; j<alphabetSize-1-i; j++){
			int idx = alphabetSize + i + i*alphabetSize + j*alphabetSize;
			if(verbose==2)
				printf("processing bucket-%d\n", idx);
			if(buckets[idx].used>1){
				int *key = (int *) malloc(buckets[idx].used * sizeof(*key));
				for(k=0; k<buckets[idx].used; k++){
					int keyIndex = buckets[idx].array[k] + 1;
					key[k] = ISA[keyIndex];
				}
				sortBucketTypeB(key, buckets[idx].array, buckets[idx].used, radixSortingMinElement, enableCuda, cudaMinElement);
				free(key);
			}
			//fill ISA
			if(buckets[idx].used>0)
				fillISA(ISA, buckets[idx].array, buckets[idx].used, buckets[idx].startPosition);
			free(buckets[idx].array);
		}
	}
	if(profiler>=1){
		clock_gettime(CLOCK_MONOTONIC, &c2);
		r1 = BILLION * (c2.tv_sec - c1.tv_sec) + c2.tv_nsec - c1.tv_nsec;
		printf("[RT]Processing buckets B= %llu nanoseconds\n", (long long unsigned int) r1);
	}

	free(bucketsB);
	free(buckets);

	//inverse ISA array to SA array
	if(profiler==2)
		clock_gettime(CLOCK_MONOTONIC, &c1);
	inverseSAISA(ISA, SA, size);
	if(profiler==2){
		clock_gettime(CLOCK_MONOTONIC, &c2);
		r1 = BILLION * (c2.tv_sec - c1.tv_sec) + c2.tv_nsec - c1.tv_nsec;
		printf("[RT]Inverse ISA to SA= %llu nanoseconds\n", (long long unsigned int) r1);
	}

	clock_gettime(CLOCK_MONOTONIC, &end);
	runtime = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
	printf("[RT]SA construction= %llu nanoseconds\n", (long long unsigned int) runtime);

	free(textInt);
}

static void initConfig(char *configFile){
	FILE *fp;
	char buff[255];

	fp = fopen(configFile, "r");
	if(fp==NULL){
		printf("File read error: %s\n", configFile);
		exit(1);
	}

	while(fgets(buff, 255, fp) != NULL){
		char *pch;
		pch = strtok (buff," ");

		if(pch!=NULL && pch[0]!='#'){
			//filepath
			if(strcmp(pch,"filepath")==0){
				pch = strtok(NULL, " \n");
				strcpy(filepath, pch);
			}
			//verbose
			if(strcmp(pch,"verbose")==0){
				pch = strtok(NULL, " ");
				verbose = (int)strtol(pch, NULL, 10);
			}
			//verify
			if(strcmp(pch,"verify")==0){
				pch = strtok(NULL, " ");
				verify = (int)strtol(pch, NULL, 10);
			}
			//profiler
			if(strcmp(pch,"profiler")==0){
				pch = strtok(NULL, " ");
				profiler = (int)strtol(pch, NULL, 10);
			}
			//lcpThreshold
			if(strcmp(pch,"lcpThreshold")==0){
				pch = strtok(NULL, " ");
				lcpThreshold = (int)strtol(pch, NULL, 10);
			}
			//enableParallelism
			if(strcmp(pch,"enableParallellism")==0){
				pch = strtok(NULL, " ");
				enableParallelism = (int)strtol(pch, NULL, 10);
			}
			//openmpMaxThread
			if(strcmp(pch,"openmpMaxThread")==0){
				pch = strtok(NULL, " ");
				openmpMaxThread = (int)strtol(pch, NULL, 10);
			}
			//parallelRadixMinElement
			if(strcmp(pch,"radixSortingMinElement")==0){
				pch = strtok(NULL, " ");
				radixSortingMinElement = (int)strtol(pch, NULL, 10);
			}
			//enableCuda
			if(strcmp(pch,"enableCuda")==0){
				pch = strtok(NULL, " ");
				enableCuda = (int)strtol(pch, NULL, 10);
			}
			//cudaMinElement
			if(strcmp(pch,"cudaMinElement")==0){
				pch = strtok(NULL, " ");
				cudaMinElement = (int)strtol(pch, NULL, 10);
			}
			//cudaBlock
			if(strcmp(pch,"cudaBlock")==0){
				pch = strtok(NULL, " ");
				cudaBlock = (int)strtol(pch, NULL, 10);
			}
			//cudaThreadPerBlock
			if(strcmp(pch,"cudaThreadPerBlock")==0){
				pch = strtok(NULL, " ");
				cudaThreadPerBlock = (int)strtol(pch, NULL, 10);
			}
		}
	}
	fclose(fp);

	if(enableParallelism)
		omp_set_num_threads(openmpMaxThread);
}

/**
 * Inverse SA/ISA(source) to ISA/SA(reverse) of lengh n
 */
void inverseSAISA(int *source, int *reverse, int n){
	int i;

	#pragma omp parallel for if(enableParallelism && n>openmpMaxThread)
	for(i=0; i<n; i++){
		int temp = source[i];
		if(temp<0) {
			printf("ERROR: FOUND ISA value less than 0\n");
			exit(1);
		}
		reverse[temp] = i;
	}
}

/**
 * Calculate maximum lcp and average lcp based on sorted SA
 */
void calculateMaxAvgLcp(char *text, int *SA, int n, int *maxLcp, float *averageLcp){
	int i;
	long average = 0;
	int max = 0;

	#pragma omp parallel for if(enableParallelism && n>openmpMaxThread)
	for(i=1; i<n; i++) {
		int cp = 0;
		int idx = SA[i];
		int idxPrev = SA[i - 1];
		while(text[idx]==text[idxPrev]){
			idx++;
			idxPrev++;
			cp++;
			if(idx>=n || idxPrev>=n)
				break;
		}
		#pragma omp critical
		{
			if (cp > max)
				max = cp;
		}
		#pragma omp atomic
		average += cp;
	}
	maxLcp[0] = max;
	averageLcp[0] = (float)average/(n-1);
}

/**
 * Fill the ISA array based on SA array respects to offset
 */
static void fillISA(int *ISA, int *SA, int saSize, int offset){
	int i;
	for(i=0; i<saSize; i++){
		int rank = SA[i];
		ISA[rank] = offset + i;
	}
}

/**
 * replace text based on |alphabet| used
 */
static int textPreprocessing(char *text, int size){
	//	1.count character used
	int charCount[MAX_ALPHABET];

	if(enableParallelism)
		characterCountOpenmp(text, size, charCount, MAX_ALPHABET);
	else
		characterCount(text, size, charCount, MAX_ALPHABET);

	if(verbose==2){
		int i;
		for(i=0; i<MAX_ALPHABET; i++){
			if(charCount[i]>0)
				printf("Char-%c = %d\n", i-128, charCount[i]);
		}
	}

	//	2. sort the used alphabet
	int alphabetSize = countNonzeroArray(charCount, MAX_ALPHABET);
	if(verbose>=1)
		printf("Alphabet used= %d\n", alphabetSize);
	char *alphabets = (char*) malloc(alphabetSize * sizeof(*alphabets));
	filterNonzeroArrayIndex(charCount, MAX_ALPHABET, alphabets);
	if(verbose==2){
		int i;
		for(i=0; i<alphabetSize; i++)
			printf("alphabet-%d = %c\n", i, alphabets[i]);
	}

	//	3. replace text
	if(enableParallelism && enableCuda && size>=cudaMinElement)
		replaceTextCuda(text, size, alphabets, alphabetSize, cudaBlock, cudaThreadPerBlock);
	else if(enableParallelism && size>openmpMaxThread)
		replaceTextOpenmp(text, size, alphabets, alphabetSize, verbose);
	else //serial
		replaceText(text, size, alphabets, alphabetSize, verbose);

	free(alphabets);
	return alphabetSize;
}

static void bucketIndexing(char *text, int size, Bucket *buckets, int bucketSize, int alphabetSize){
	int i;
	//init buckets
	for(i=0; i<bucketSize; i++){
		Bucket bucket;
		if(i!=bucketSize-1)
			initBucket(&bucket, ARRAY_GROW);
		else
			initBucket(&bucket, 1); //bucket containing last character, it has no next character
		buckets[i] = bucket;
	}

	if(enableParallelism && size>openmpMaxThread)
		buildBucketIndexOpenmp(text, size, buckets, bucketSize, alphabetSize);
	else
		buildBucketIndex(text, size, buckets, bucketSize, alphabetSize);

	int lastCharInsertion = (text[size-1]+128) * alphabetSize;
	int sum = 0;
	for(i=0; i<bucketSize-1; i++){
		if(i==lastCharInsertion){
			buckets[bucketSize-1].startPosition = sum;
			sum += 1;
			if(verbose==2)
				printf("bucket-%d has %d elements\n", bucketSize-1, buckets[bucketSize-1].used);
		}
		if(verbose==2 && buckets[i].used>0)
			printf("bucket-%d has %d elements\n", i, buckets[i].used);
		buckets[i].startPosition = sum;
		sum += buckets[i].used;
	}
}

/*
 * Copyright (c) 2003-2008 Yuta Mori All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 * Check the accuracy of suffix array of text T
 */
void sufcheck(char *T, int *SA, int n) {
	int ALPHABET_SIZE = 256;
	int C[ALPHABET_SIZE];
	int i, p, q, t;
	char c;
	int k;

	if (verbose) {
		fprintf(stderr, "sufcheck: \n");
	}

	/* Check arguments. */
	if ((T == NULL) || (SA == NULL) || (n < 0)) {
		if (verbose) {
			fprintf(stderr, "Invalid arguments.\n");
		}
	}
	if (n == 0) {
		if (verbose) {
			fprintf(stderr, "Done.\n");
		}
	}

	/* check range: [0..n-1] */
	k = 0;
	for (i = 0; i < n; ++i) {
		if ((SA[i] < 0) || (n <= SA[i])) {
			k++;
			if (verbose)
				fprintf(stderr, "Out of the range [0,%d].\n  SA[%d]=%d\n", n-1, i, SA[i]);
		}
	}
	fprintf(stderr, "%d suffixes out of the range.\n", k);

	/* check first characters. */
	k = 0;
	for (i = 1; i < n; ++i) {
		if (T[SA[i - 1]] > T[SA[i]]) {
			k++;
			if (verbose==2) {
				fprintf(stderr, "Suffixes in wrong order.\n  T[SA[%d]=%d]=%d > T[SA[%d]=%d]=%d\n",
						i - 1, SA[i - 1], T[SA[i - 1]], i, SA[i], T[SA[i]]);
			}
		}
	}
	fprintf(stderr, "%d suffixes in wrong order (first character comparison).\n", k);

	/* check suffixes. */
	for (i = 0; i < ALPHABET_SIZE; ++i) {
		C[i] = 0;
	}
	for (i = 0; i < n; ++i) {
		++C[(int)T[i]+128];
	}
	for (i = 0, p = 0; i < ALPHABET_SIZE; ++i) {
		t = C[i];
		C[i] = p;
		p += t;
	}

	k = 0;
	q = C[(int)T[n - 1]+128];
	C[(int)T[n - 1]+128] += 1;
	for (i = 0; i < n; ++i) {
		p = SA[i];
		if (0 < p) {
			c = T[--p];
			t = C[(int)c+128];
		} else {
			c = T[p = n - 1];
			t = q;
		}
		if ((t < 0) || (p != SA[t])) {
			k++;
			if (verbose==2) {
				fprintf(stderr, "Suffix in wrong position.\n"
								"  SA[%d]=%d or\n"
								"  SA[%d]=%d\n",
						t, (0 <= t) ? SA[t] : -1, i, SA[i]);
			}
		}
		if (t != q) {
			++C[(int)c+128];
			if ((n <= C[(int)c+128]) || (T[SA[C[(int)c+128]]] != c)) {
				C[(int)c+128] = -1;
			}
		}
	}
	fprintf(stderr, "%d suffixes in wrong position.\n", k);

	if (1 <= verbose) {
		fprintf(stderr, "Done.\n");
	}
}