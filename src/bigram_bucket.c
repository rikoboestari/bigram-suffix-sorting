/*
 * bigram_bucket.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdio.h>
#include <stdlib.h>

#include "bucket.h"
#include "growing_array.h"
#include "sort.h"
#include "sa_kernel.h"
#include "repetition.h"

#define UPDATE_KEY_PARALLEL_MIN 100000


static void bucketSort(int textSize, int *textInt, int *keys, int *values, int n, int h, const int depth,
                       int radixSortingMinElement, int enableParallelism, int enableCuda, int cudaMinElement);

/**
 * Initialize bucket with size = initialSize
 */
void initBucket(Bucket *bucket, int initialSize) {
    bucket->array = (int*)malloc(initialSize * sizeof(bucket->array));
    if(bucket->array==NULL){
		printf("Memory allocation error\n");
		exit(1);
	}
    bucket->used = 0;
    bucket->size = initialSize;
}

/**
 * Insert element into bucket. If bucket array is full, it grows the array size first using realloc
 * then insert the element
 */
static void insertToBucket(Bucket *bucket, int element) {
    if (bucket->used == bucket->size) {
        bucket->size += ARRAY_GROW;
        int *_tmp = (int *)realloc(bucket->array, bucket->size * sizeof(*_tmp));
        if(_tmp==NULL){
            printf("Memory reallocation error\n");
            exit(EXIT_FAILURE);
        }
        bucket->array = _tmp;
    }
	bucket->array[bucket->used] = element;
    bucket->used += 1;
}

/**
 * Build the bigram bucket index.
 * If you have 4 alphabets (A,B,C,D), then
 * AA will go to bucket-0	BA will go to bucket-4
 * AB will go to bucket-1	BB will go to bucket-5
 * AC will go to bucket-2	BC will go to bucket-6
 * AD will go to bucket-3	BD will go to bucket-7
 * and so on...
 */
void buildBucketIndex(char* text, int textSize, Bucket buckets[], int bucketsSize, int alphabetSize){
	int i;
	for(i=0; i<textSize; i++){
		int bucketNumber = bucketsSize-1;
		if(i!=textSize-1)
			bucketNumber = (text[i]+128)*alphabetSize + (text[i+1]+128);
		insertToBucket(&buckets[bucketNumber], i);
	}
}

void buildBucketIndexOpenmp(char* text, int textSize, Bucket buckets[], int bucketsSize, int alphabetSize){
    int *bucketNumber = (int*) malloc (textSize * sizeof(int));
    int i;

    #pragma omp parallel for
    for(i=0; i<textSize; i++){
        int temp;
        if(i!=textSize-1)
            temp = (text[i] + 128) * alphabetSize + (text[i + 1] + 128);
        else
            temp = bucketsSize - 1;
        bucketNumber[i] = temp;
    }

    for(i=0; i<textSize; i++){
        insertToBucket(&buckets[bucketNumber[i]], i);
    }

    free(bucketNumber);
}

/**
 * Sort the suffixes in a bucket
 */
void sortBucketTypeA(int textSize, int *textInt, int *suffixIndex, int suffixIndexSize, int h, const int depth,
                     int radixSortingMinElement, int enableParallelism, int enableCuda, int cudaMinElement){
    int *keys = (int*) malloc (suffixIndexSize * sizeof(*keys));
    bucketSort(textSize, textInt, keys, suffixIndex, suffixIndexSize, h, depth, radixSortingMinElement, enableParallelism,
               enableCuda, cudaMinElement);
    free(keys);
}

static void bucketSort(int textSize, int *textInt, int *keys, int *values, int n, int h, const int depth,
                       int radixSortingMinElement, int enableParallelism, int enableCuda, int cudaMinElement){
    GrowingArray partitionIndex;
    GrowingArray partitionSize;
    initArray(&partitionIndex);
    initArray(&partitionSize);
    insertToArray(&partitionIndex, 0);
    insertToArray(&partitionSize, n);

    while (partitionIndex.used>0) {
        int i;

        for(i=0; i<partitionIndex.used; i++){
            int idx = partitionIndex.array[i];
            int size = partitionSize.array[i];

            Repetition repetition;
            repetition.patternLength = 0;
            detectRepetition(textInt, textSize, values+idx, size, h, depth, &repetition);
            if(repetition.patternLength > 0){
                //sort the repetition
                sortRepetition(&repetition, textInt, textSize, values+idx, size, h, depth);

                partitionSize.array[i] = 0;
                freeRepetition(&repetition);
            }else{
                //update keys
                int j;
                for(j=0; j<size; j++){
                    if(values[idx + j] + h < textSize)
                        keys[idx + j] = textInt[values[idx + j] + h];
                    else
                        keys[idx + j] = 0;
                }

                //sort
                if (enableParallelism && enableCuda && size >= cudaMinElement) {
                    sortKVCuda(keys + idx, values + idx, size);
                } else if (size >= radixSortingMinElement) {
                    radixKVSort(keys + idx, values + idx, size);
                } else {
                    //quickSortKeysValuesPairs(keys+partIdx, values+partIdx, partSize);
                    mergesortKV(keys + idx, values + idx, size);
                }
            }
        }

        //update partitionIndex & partitionSize
        int *copyIndex, *copySize, nbCopy;
        nbCopy = partitionIndex.used;
        copyIndex = (int*) malloc (nbCopy * sizeof(copyIndex));
        copySize = (int*) malloc (nbCopy * sizeof(copySize));

        for(i=0; i<partitionIndex.used; i++){
            copyIndex[i] = partitionIndex.array[i];
            copySize[i] = partitionSize.array[i];
        }
        resetArray(&partitionIndex);
        resetArray(&partitionSize);

        int newOffset = -1, newN = 0;
        int previousKey=0;

        for(i=0; i<nbCopy; i++){
            int index = copyIndex[i];
            int nb = copySize[i];
            int j;
            for(j=index; j<index+nb; j++){
                if(j==index){
                    previousKey = keys[j];
                    newOffset = j;
                    newN = 1;
                }else{
                    if(keys[j]==previousKey){
                        newN++;
                    }else{
                        if(newN>1){
                            insertToArray(&partitionIndex, newOffset);
                            insertToArray(&partitionSize, newN);
                            previousKey = keys[j];
                            newOffset = j;
                            newN = 1;
                        }else{
                            previousKey = keys[j];
                            newOffset = j;
                            newN = 1;
                        }
                    }
                }
            }
            if(newN>1){
                insertToArray(&partitionIndex, newOffset);
                insertToArray(&partitionSize, newN);
            }
        }
        free(copyIndex);
        free(copySize);

        h += depth;
    }
    destroyArray(&partitionIndex);
    destroyArray(&partitionSize);
}

void sortBucketTypeB(int *keys, int *values, int n, int radixSortingMinElement, int enableCuda, int cudaMinElement){
    if(enableCuda && n>=cudaMinElement) {
        sortKVCuda(keys, values, n);
    }else if(n >= radixSortingMinElement) {
        radixKVSort(keys, values, n);
    }else {
        quickSortKeysValuesPairs(keys, values, n);
    }
}
