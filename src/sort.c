/*
 * sort.c for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

#include "sort.h"


/**
 * Sort array of length n using quick sort algorithm
 */
void quickSort(int *array, int n){
	int i, j, p, t;
	if (n < 2)
		return;
	p = array[n / 2];
	for (i = 0, j = n - 1;; i++, j--) {
		while (array[i] < p)
			i++;
		while (p < array[j])
			j--;
		if (i >= j)
			break;
		t = array[i];
		array[i] = array[j];
		array[j] = t;
	}
	quickSort(array, i);
	quickSort(array + i, n - i);
}

/**
 * Sort keys, values array pairs based on its keys, using modified quick sort algorithm
 */
void quickSortKeysValuesPairs(int *key, int *value, int n) {
	int i, j, p, t, u;
	if (n < 2)
		return;
	p = key[n / 2];
	for (i = 0, j = n - 1;; i++, j--) {
		while (key[i] < p)
			i++;
		while (p < key[j])
			j--;
		if (i >= j)
			break;
		t = key[i];
		u = value[i];
		key[i] = key[j];
		value[i] = value[j];
		key[j] = t;
		value[j] = u;
	}
	quickSortKeysValuesPairs(key, value, i);
	quickSortKeysValuesPairs(key + i, value + i, n - i);
}

/**
 * Modified version of OpenMP implementation radix sort by Haichuan Wang.
 * https://haichuanwang.wordpress.com/2014/05/26/a-faster-openmp-radix-sort-implementation/
 */
void radixKVSort(int *keys, int *values, int n) {
	int *bufferKeys = (int *) malloc (n * sizeof(int));
	int *bufferValues = (int *) malloc (n * sizeof(int));
	int total_digits = sizeof(int)*8;

	//Each thread use local_bucket to move k&v
	int i;
	for(int shift = 0; shift < total_digits; shift+=BASE_BITS) {
		int bucket[BASE] = {0};

		int local_bucket[BASE] = {0}; // size needed in each bucket/thread
		//1st pass, scan whole and check the count
//		#pragma omp parallel firstprivate(local_bucket)
		{
//			#pragma omp for schedule(static) nowait
			for(i = 0; i < n; i++){
				local_bucket[DIGITS(keys[i], shift)]++;
			}
//			#pragma omp critical
			for(i = 0; i < BASE; i++) {
				bucket[i] += local_bucket[i];
			}
//			#pragma omp barrier
//			#pragma omp single
			for (i = 1; i < BASE; i++) {
				bucket[i] += bucket[i - 1];
			}
			int nthreads = omp_get_num_threads();
			int tid = omp_get_thread_num();
			for(int cur_t = nthreads - 1; cur_t >= 0; cur_t--) {
				if(cur_t == tid) {
					for(i = 0; i < BASE; i++) {
						bucket[i] -= local_bucket[i];
						local_bucket[i] = bucket[i];
					}
				} else { //just do barrier
//					#pragma omp barrier
				}

			}
//			#pragma omp for schedule(static)
			for(i = 0; i < n; i++) { //note here the end condition
				bufferKeys[local_bucket[DIGITS(keys[i], shift)]] = keys[i];
				bufferValues[local_bucket[DIGITS(keys[i], shift)]] = values[i];
				local_bucket[DIGITS(keys[i], shift)]++;
			}
		}
		//now move k&v
		int* tmpKeys = keys;
		int *tmpValues = values;
		keys = bufferKeys;
		values = bufferValues;
		bufferKeys = tmpKeys;
		bufferValues = tmpValues;
	}
	free(bufferKeys);
	free(bufferValues);
}

void countingKVSort(char *keys, int *values, int n, int min, int max){
	int i;
	int range = max - min + 1;
	int count[range];


	for(i = 0; i < range; i++)
		count[i] = 0;

	char *bufKeys = (char*) malloc (n * sizeof(char));
	int *bufValues = (int*) malloc (n * sizeof(int));

	for(i = 0; i < n; i++) {
		count[keys[i]+128]++;
		bufKeys[i] = keys[i];
		bufValues[i] = values[i];
	}

	for(i=1; i<range; i++){
		count[i] += count[i-1];
	}

	for(i=n-1; i>=0; i--){
		int pos = --count[bufKeys[i]+128];
		keys[pos] = bufKeys[i];
		values[pos] = bufValues[i];
	}
	free(bufKeys);
	free(bufValues);
}


static void mergeKV(int *keys, int *values, int size, int a_size, int b_size){
	int i, j, k;
	int *keysA = keys, *keysB = keys + a_size;
	int *keysC;
	int *valuesA = values, *valuesB = values + a_size;
	int *valuesC;

	if (keysA[a_size-1] <= keysB[0])
		return;

	keysC = (int *)malloc(b_size * sizeof(int));
	memcpy(keysC, keysB, b_size * sizeof(int));
	valuesC = (int *)malloc(b_size * sizeof(int));
	memcpy(valuesC, valuesB, b_size * sizeof(int));

	i = a_size - 1; j = b_size - 1; k = size - 1;
	while (i >= 0 && j >= 0)
	{
		if (keysA[i] > keysC[j]) {
			keys[k] = keysA[i];
			values[k] = valuesA[i];
			k--; i--;
		}else{
			keys[k] = keysC[j];
			values[k] = valuesC[j];
			k--; j--;
		}
	}
	while (j >= 0) {
		keys[k] = keysC[j];
		values[k] = valuesC[j];
		k--; j--;
	}
	free(keysC);
	free(valuesC);
}

void mergesortKV(int *keys, int *values, int size){
	int a_size = size / 2;
	int b_size = size - a_size;

	if (size <= 1)
		return;

	mergesortKV(keys, values, a_size);
	mergesortKV(keys + a_size, values + a_size, b_size);

	mergeKV(keys, values, size, a_size, b_size);
}