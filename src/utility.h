/*
 * utility.h for bigram-suffix-sorting
 * Copyright (c) 2016 Riko Boestari All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


#ifndef SUFFIXARRAY_UTILITY_H
#define SUFFIXARRAY_UTILITY_H

void loadFile(char *filename, char *buffer, long size);

void characterCount(char *text, int textSize, int charCount[], int charCountSize);

void characterCountOpenmp(char *text, int textSize, int charCount[], int charCountSize);

int countNonzeroArray(int array[], int size);

void filterNonzeroArrayIndex(int input[], int size, char *output);

int getExponent(int alphabetSize);

int maxDepth(int exponent);

void replaceText(char *text, int textSize, char *alphabets, int alphabetSize, int verbose);

void replaceTextOpenmp(char *text, int textSize, char *alphabets, int alphabetSize, int verbose);

void calculateIntegerRepresentation(const char *text, int textSize, int exponent, const int depth,
                                   int *textInt, int enableParallellism);

void reverseArray(int *array, int n);

#endif //SUFFIXARRAY_UTILITY_H
